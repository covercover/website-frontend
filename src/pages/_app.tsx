import "../lib/pageEvents";
import "../styles/index.css";

import ApolloClient from "apollo-client";
import App, { Container } from "next/app";
import React from "react";
import { ApolloProvider } from "react-apollo";
import styled from "styled-components";

import Head from "next/head";
import LinkToSources from "../components/LinkToSource";
import LocaleToggler from "../components/LocaleToggler";
import { appWithTranslation } from "../i18n";
import withApolloClient from "../lib/withApolloClient";

const Wrapper = styled.div`
  display: table;
  width: 100%;
  height: 100%;
  position: relative;
`;

class MyApp extends App<{ apolloClient: ApolloClient<{}> }> {
  public static async getInitialProps({ Component, ctx }) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    const { hostByLocale } = ctx.req || (window as any).__NEXT_DATA__.props;

    return {
      hostByLocale,
      pageProps: { ...pageProps, hostByLocale },
    };
  }

  public render() {
    const { Component, pageProps, apolloClient } = this.props;
    return (
      <Container>
        <Head>
          <meta name="viewport" content="width=500" />
        </Head>
        <ApolloProvider client={apolloClient}>
          <Wrapper>
            <LocaleToggler {...pageProps} />
            <Component {...pageProps} />
            <LinkToSources />
          </Wrapper>
        </ApolloProvider>
      </Container>
    );
  }
}

export default withApolloClient(appWithTranslation(MyApp));
