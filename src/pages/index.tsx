import Head from "next/head";
import React from "react";
import styled from "styled-components";

import Description from "../components/Description";
import H1 from "../components/H1";
import Photo from "../components/Photo";
import Profiles from "../components/Profiles";
import VerticallyCentered from "../components/VerticallyCentered";
import { I18nPage, includeDefaultNamespaces, withNamespaces } from "../i18n";

const LastName = styled.span`
  display: inline-block;
  position: relative;
`;

const Wrapper = styled.div`
  width: 450px;
  margin: 0 auto 10px;
  padding-top: 10px;
`;

const LastNamePronunciation = styled.div`
  position: absolute;
  font-size: 13px;
  bottom: -20px;
  left: 0;
  right: 0;
  text-align: center;
  opacity: 0.55;
  font-weight: normal;
`;
const LastNamePronunciationBracket = styled.span`
  display: none;
`;

const Page: I18nPage = ({ t, i18n }) => (
  <VerticallyCentered>
    <Wrapper>
      <Head>
        <title>{t("title")}</title>
        <meta name="description" content={t("description")} />
      </Head>
      <H1>
        {t("h1.firstName")}{" "}
        <LastName>
          {t("h1.lastName")}
          {i18n.language === "en" ? (
            <>
              {" "}
              <LastNamePronunciation key="last-name">
                <LastNamePronunciationBracket>(</LastNamePronunciationBracket>
                catch · ka ′ yev
                <LastNamePronunciationBracket>)</LastNamePronunciationBracket>
              </LastNamePronunciation>
            </>
          ) : null}
        </LastName>
      </H1>
      <Description />
      <Photo />
      <Profiles />
    </Wrapper>
  </VerticallyCentered>
);

Page.getInitialProps = () => {
  return {
    namespacesRequired: includeDefaultNamespaces(["index"]),
  };
};

export default withNamespaces(["index", "common"])(Page);
