import Head from "next/head";
import Link from "next/link";
import React from "react";
import styled from "styled-components";

import Explanation from "../components/Explanation";
import H1 from "../components/H1";
import PhotoSample from "../components/PhotoSample";
import VerticallyCentered from "../components/VerticallyCentered";
import {
  I18nPage,
  Trans,
  includeDefaultNamespaces,
  withNamespaces,
} from "../i18n";

const Ul = styled.ul`
  list-style: none;
  padding-left: 20px;
  margin-bottom: 1.5em;

  & li {
    margin-top: 1.5em;
  }
`;

const Page: I18nPage = ({ t, i18n }) => (
  <VerticallyCentered>
    <Head>
      <title>{t("title")}</title>
    </Head>
    <H1 error={true}>{t("h1")}</H1>
    <PhotoSample />
    <Explanation>
      <Trans i18nKey="explanation">
        <a href="https://www.flickr.com/people/kachkaev">flickr</a>
      </Trans>
      <Ul>
        <li>
          <Trans i18nKey="hint1">
            <a
              href={`https://${
                i18n.language
              }.wikipedia.org/wiki/Creative_Commons`}
            >
              cc
            </a>
          </Trans>
        </li>
        <li>
          <Trans i18nKey="hint2" />
        </li>
        <li>
          <Trans i18nKey="hint3">
            <a href="mailto:alexander@kachkaev.ru">email</a>
          </Trans>
        </li>
      </Ul>
      <Link href="/">
        <a>{t("common:signature")}</a>
      </Link>
    </Explanation>
  </VerticallyCentered>
);

Page.getInitialProps = () => {
  return {
    namespacesRequired: includeDefaultNamespaces(["photos"]),
  };
};

export default withNamespaces(["photos", "common"])(Page);
