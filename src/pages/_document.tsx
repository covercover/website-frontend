import { readFileSync } from "fs";
import { resolve } from "path";
import _ from "lodash";
import Document, { Head, Main, NextScript } from "next/document";
import React from "react";
import { ServerStyleSheet } from "styled-components";

import removeCommentsAndSpacing from "../lib/removeCommentsAndSpacing";

const nextDir = resolve(process.cwd(), ".next");
const doGetContent = (file) => readFileSync(resolve(nextDir, file), "utf8");
const getContent =
  process.env.NODE_ENV === "production"
    ? _.memoize(doGetContent)
    : doGetContent;
class InlineStylesHead extends Head {
  public getCssLinks() {
    return this.__getInlineStyles();
  }

  public __getInlineStyles() {
    const { assetPrefix, files } = (this as any).context._documentProps;
    if (!files || files.length === 0) {
      return null;
    }

    return files
      .filter((file) => /\.css$/.test(file))
      .map((file) => (
        <style
          key={file}
          nonce={this.props.nonce}
          data-href={`${assetPrefix}/_next/${file}`}
          dangerouslySetInnerHTML={{
            __html: getContent(file),
          }}
        />
      ));
  }
}

export default class MyDocument extends Document<{
  styleTags;
  gaTrackingId;
  locale;
}> {
  public static getInitialProps({ renderPage, req }: { renderPage; req? }) {
    const sheet = new ServerStyleSheet();
    const page = renderPage((App) => (props) =>
      sheet.collectStyles(<App {...props} />),
    );
    const styleTags = sheet.getStyleElement();

    return {
      ...page,
      locale: req.locale,
      styleTags,
      gaTrackingId: req.gaTrackingId,
      hostByLocale: req.hostByLocale,
    };
  }

  public render() {
    return (
      <html lang={this.props.locale}>
        <InlineStylesHead>
          <meta charSet="utf-8" />
          <link rel="preconnect" href="https://www.google-analytics.com" />
          <meta
            name="apple-mobile-web-app-status-bar-style"
            content="black-translucent"
          />
          <link
            rel="apple-touch-icon"
            sizes="57x57"
            href="/static/favicon/apple-icon-57x57.png"
          />
          <link
            rel="apple-touch-icon"
            sizes="60x60"
            href="/static/favicon/apple-icon-60x60.png"
          />
          <link
            rel="apple-touch-icon"
            sizes="72x72"
            href="/static/favicon/apple-icon-72x72.png"
          />
          <link
            rel="apple-touch-icon"
            sizes="76x76"
            href="/static/favicon/apple-icon-76x76.png"
          />
          <link
            rel="apple-touch-icon"
            sizes="114x114"
            href="/static/favicon/apple-icon-114x114.png"
          />
          <link
            rel="apple-touch-icon"
            sizes="120x120"
            href="/static/favicon/apple-icon-120x120.png"
          />
          <link
            rel="apple-touch-icon"
            sizes="144x144"
            href="/static/favicon/apple-icon-144x144.png"
          />
          <link
            rel="apple-touch-icon"
            sizes="152x152"
            href="/static/favicon/apple-icon-152x152.png"
          />
          <link
            rel="apple-touch-icon"
            sizes="180x180"
            href="/static/favicon/apple-icon-180x180.png"
          />
          <link
            rel="icon"
            type="image/png"
            sizes="192x192"
            href="/static/favicon/android-icon-192x192.png"
          />
          <link
            rel="icon"
            type="image/png"
            sizes="32x32"
            href="/static/favicon/favicon-32x32.png"
          />
          <link
            rel="icon"
            type="image/png"
            sizes="96x96"
            href="/static/favicon/favicon-96x96.png"
          />
          <link
            rel="icon"
            type="image/png"
            sizes="16x16"
            href="/static/favicon/favicon-16x16.png"
          />
          <link rel="manifest" href="/manifest.json" />
          <meta name="msapplication-TileColor" content="#ffffff" />
          <meta
            name="msapplication-TileImage"
            content="/static/favicon/ms-icon-144x144.png"
          />
          <meta name="theme-color" content="#ffffff" />
          {this.props.styleTags}
          {this.props.gaTrackingId ? (
            [
              <script
                key="ga1"
                async={true}
                src={`https://www.googletagmanager.com/gtag/js?id=${
                  this.props.gaTrackingId
                }`}
              />,
              <script
                key="ga2"
                dangerouslySetInnerHTML={{
                  __html: removeCommentsAndSpacing(`
window.dataLayer = window.dataLayer || [];
window.gaTrackingId = '${this.props.gaTrackingId}';
function gtag(){
  dataLayer.push(arguments)
}
gtag('js', new Date());
gtag('config', window.gaTrackingId);`),
                }}
              />,
            ]
          ) : (
            <script
              dangerouslySetInnerHTML={{
                __html: removeCommentsAndSpacing(`
function gtag(){
  console.log('dummy gtag call', arguments)
}`),
              }}
            />
          )}
        </InlineStylesHead>
        <body>
          <div className="next-main">
            <Main />
          </div>
          <NextScript />
        </body>
      </html>
    );
  }
}
