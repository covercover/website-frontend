import Head from "next/head";
import Link from "next/link";
import React from "react";

import Explanation from "../components/Explanation";
import H1 from "../components/H1";
import VerticallyCentered from "../components/VerticallyCentered";
import {
  I18nPage,
  Trans,
  includeDefaultNamespaces,
  withNamespaces,
} from "../i18n";

const Page: I18nPage = ({ t }) => (
  <VerticallyCentered>
    <Head>
      <title>{t("title")}</title>
    </Head>
    <H1 error={true}>{t("h1")}</H1>
    <Explanation>
      <Trans i18nKey="explanation">
        <a href="mailto:alexander@kachkaev.ru">email</a>
      </Trans>
      <br />
      <br />
      <Link href="/">
        <a>{t("common:signature")}</a>
      </Link>
    </Explanation>
  </VerticallyCentered>
);

Page.getInitialProps = () => {
  return {
    namespacesRequired: includeDefaultNamespaces([]),
  };
};

export default withNamespaces(["_error", "common"])(Page);
