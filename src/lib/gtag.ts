/* eslint-disable @typescript-eslint/camelcase */

declare let window: Window & {
  gtag: any;
  gaTrackingId?: string;
};

// https://developers.google.com/analytics/devguides/collection/gtagjs/pages
export const logPageView = () => {
  if (window.gtag) {
    window.gtag("config", window.gaTrackingId, {
      page_location: window.location.href,
      page_path: window.location.pathname,
      page_title: window.document.title,
    });
  }
};

// https://developers.google.com/analytics/devguides/collection/gtagjs/events
export const logEvent = ({
  action,
  category,
  label,
  value,
}: {
  category: string;
  action: string;
  label?: string;
  value?: string;
}) => {
  if (window.gtag) {
    window.gtag("event", action, {
      event_category: category,
      event_label: label,
      value,
    });
  }
};

export const reportError = (error: Error, category?: string) => {
  if (!window.gtag) {
    return;
  }
  window.gtag("event", "error", {
    event_label: error.stack || error.message,
    event_category: category,
  });
};

// error reporting
if (typeof window !== "undefined") {
  const oldOnError = window.onerror;
  window.onerror = (
    message: string,
    url: string,
    lineNo?: number,
    columnNo?: number,
    error?: Error,
  ) => {
    let normalizedError = error;
    if (!normalizedError) {
      normalizedError = new Error(message);
      normalizedError.stack = `${message}\n at ${url}:${lineNo}:${columnNo}`;
    }
    reportError(normalizedError, "window");
    if (oldOnError) {
      oldOnError.apply(this, [message, url, lineNo, columnNo, error]);
    }
  };
}
