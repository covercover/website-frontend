import gql from "graphql-tag";
import _ from "lodash";
import React, { FunctionComponent } from "react";
import { Query } from "react-apollo";
import styled from "styled-components";

import { WithNamespaces, withNamespaces } from "../i18n";
import mergeTwitterProfiles from "../lib/mergeTwitterProfiles";
import FlickrPhotos from "./FlickrPhotos";
import KeyProfileDescription from "./KeyProfileDescription";
import KeyProfileName from "./KeyProfileName";
import SimpleProfile from "./SimpleProfile";

const Wrapper = styled.div`
  text-align: left;
`;
const KeyProfile = styled.div`
  min-height: 50px;
`;
const KeyProfileHeader = styled.div``;

const SimpleProfiles = styled.div<{ afterRemark?: boolean }>`
  margin-top: ${(p) => (p.afterRemark ? 15 : 25)}px;
  text-align: center;
`;
const SimpleProfileRemark = styled.div`
  opacity: 0.55;
`;

const ProfilesView: FunctionComponent<
  WithNamespaces & {
    profilesByName: any;
    children?: never;
  }
> = ({ profilesByName, t, i18n }) => {
  return (
    <Wrapper>
      <KeyProfile>
        <KeyProfileHeader>
          <KeyProfileName profileName="openaccess" />
          <a href="http://openaccess.city.ac.uk/view/creators_id/alexander=2Ekachkaev=2E1.html">
            Kachkaev, A.
          </a>
        </KeyProfileHeader>
        <KeyProfileDescription
          profileName="openaccess"
          profilesByName={profilesByName}
        >
          <a href="http://openaccess.city.ac.uk/12460/">phd</a>
        </KeyProfileDescription>
      </KeyProfile>
      <KeyProfile>
        <KeyProfileHeader>
          <KeyProfileName profileName="linkedin" />
          <a href="https://www.linkedin.com/in/kachkaev/">kachkaev</a>
        </KeyProfileHeader>
        <KeyProfileDescription
          profileName="linkedin"
          profilesByName={profilesByName}
        />
      </KeyProfile>
      <KeyProfile>
        <KeyProfileHeader>
          <KeyProfileName profileName="github" />
          <a href="https://github.com/kachkaev">kachkaev</a>
        </KeyProfileHeader>
        <KeyProfileDescription
          profileName="github"
          profilesByName={profilesByName}
        >
          <a href="https://github.com/kachkaev?utf8=✓&tab=repositories&q=stars%3A>%3D0&type=source">
            sources
          </a>
        </KeyProfileDescription>
      </KeyProfile>
      <KeyProfile>
        <KeyProfileHeader>
          <KeyProfileName profileName="gitlab" />
          <a href="https://gitlab.com/kachkaev">kachkaev</a>
        </KeyProfileHeader>
        <KeyProfileDescription
          profileName="gitlab"
          profilesByName={profilesByName}
        >
          <a href="https://gitlab.com/kachkaev/website">this website</a>
          <a href="https://gitlab.kachkaev.ru/">personal instance</a>
        </KeyProfileDescription>
      </KeyProfile>
      <KeyProfile>
        <KeyProfileHeader>
          <KeyProfileName profileName="osm" />
          <a href="https://www.openstreetmap.org/user/Kachkaev">Kachkaev</a>
        </KeyProfileHeader>
        <KeyProfileDescription
          profileName="osm"
          profilesByName={profilesByName}
        >
          <a href="http://yosmhm.neis-one.org/?u=Kachkaev">area</a>
        </KeyProfileDescription>
      </KeyProfile>
      <KeyProfile>
        <KeyProfileHeader>
          <KeyProfileName profileName="twitter" />
          <a href="https://twitter.com/kachkaev">kachkaev</a>,{" "}
          <a href="https://twitter.com/kachkaev_ru">kachkaev_ru</a>
        </KeyProfileHeader>
        <KeyProfileDescription
          profileName="twitter"
          profilesByName={profilesByName}
        />
      </KeyProfile>
      <KeyProfile>
        <KeyProfileHeader>
          <KeyProfileName profileName="flickr" />
          <a href="https://www.flickr.com/people/kachkaev">kachkaev</a>
        </KeyProfileHeader>
        <KeyProfileDescription
          profileName="flickr"
          profilesByName={profilesByName}
        />
        <FlickrPhotos {...profilesByName["flickr"]} />
      </KeyProfile>
      <SimpleProfiles>
        <SimpleProfile
          href="https://www.facebook.com/kachkaev"
          name={t("profiles.facebook.name")}
        />{" "}
        <SimpleProfile
          href="https://vk.com/kachkaev"
          name={t("profiles.vk.name")}
        />{" "}
        <SimpleProfile
          href="https://meta.stackoverflow.com/users/1818285/alexander-kachkaev"
          name={t("profiles.stackoverflow.name")}
        />
        {i18n.language === "ru" ? (
          <>
            {" "}
            <SimpleProfile
              href="https://habr.com/users/kachkaev/"
              name={t("profiles.habrahabr.name")}
            />{" "}
            <SimpleProfile
              key="wikipedia-ru"
              href="https://ru.wikipedia.org/wiki/Участник:Kachkaev"
              name={t("profiles.wikipedia.name")}
            />
          </>
        ) : null}
      </SimpleProfiles>
      <SimpleProfiles>
        <SimpleProfile
          href="https://www.instagram.com/alexanderkachkaev/"
          name={t("profiles.instagram.name")}
          notUsed={true}
        />{" "}
        <SimpleProfile
          href="https://plus.google.com/+AlexanderKachkaev"
          name={t("profiles.googleplus.name")}
          notUsed={true}
        />{" "}
        <SimpleProfile
          href="https://www.periscope.tv/kachkaev"
          name={t("profiles.periscope.name")}
          notUsed={true}
        />
        <SimpleProfileRemark>{t("notUsedProfilesRemark")}</SimpleProfileRemark>
      </SimpleProfiles>
      <SimpleProfiles afterRemark={true}>
        <SimpleProfile
          href="mailto:alexander@kachkaev.ru"
          name="alexander@kachkaev.ru"
        />
        <SimpleProfileRemark>{t("emailRemark")}</SimpleProfileRemark>
      </SimpleProfiles>
    </Wrapper>
  );
};

const MemoizedProfilesView = React.memo(
  withNamespaces(["index", "common"])(ProfilesView),
);

const profiles = gql`
  query ProfileInfos {
    profiles(onlyWithInfo: true) {
      name
      info
    }
  }
`;

const Profiles: FunctionComponent<{ children?: never }> = () => {
  return (
    <Query query={profiles} fetchPolicy="cache-first">
      {({ data }) => (
        <MemoizedProfilesView
          profilesByName={mergeTwitterProfiles(
            _.keyBy((data || {}).profiles, (profile) => profile.name),
          )}
        />
      )}
    </Query>
  );
};

export default Profiles;
