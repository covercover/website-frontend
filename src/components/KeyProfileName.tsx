import React, { FunctionComponent } from "react";
import styled from "styled-components";

import { Trans } from "../i18n";

const Bold = styled.span`
  font-weight: bold;
`;

const KeyProfileName: FunctionComponent<{
  profileName: string;
  children?: never;
}> = ({ profileName }) => {
  return (
    <>
      <Bold>
        <Trans i18nKey={`profiles.${profileName}.name`} />
      </Bold>
      :{" "}
    </>
  );
};

export default KeyProfileName;
