import { format } from "url";
import { UrlLike } from "next/router";
import React, { FunctionComponent } from "react";
import styled from "styled-components";

import { WithNamespaces, withNamespaces } from "../i18n";

const CurrentLocale = styled.span`
  margin-left: 8px;
`;
const LinkToLocale = styled.a`
  margin-left: 8px;
`;

const Locale: FunctionComponent<
  {
    targetLocale: string;
    host: string;
    currentUrl: UrlLike;
    children?: never;
  } & WithNamespaces
> = ({ targetLocale, i18n, host, currentUrl }) => {
  if (targetLocale === i18n.language) {
    return <CurrentLocale key={targetLocale}>{targetLocale}</CurrentLocale>;
  }

  return (
    <LinkToLocale
      key={targetLocale}
      href={format({ protocol: "https:", ...currentUrl, hostname: host })}
    >
      {targetLocale}
    </LinkToLocale>
  );
};

export default withNamespaces([])(Locale);
