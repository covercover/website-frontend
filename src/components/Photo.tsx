import React, { FunctionComponent } from "react";
import styled from "styled-components";

import { WithNamespaces, withNamespaces } from "../i18n";

const Wrapper = styled.div`
  float: right;
`;
const Img = styled.img`
  display: block;
  border-radius: 5px;
  color: #fff;
`;

const Photo: FunctionComponent<WithNamespaces & { children?: never }> = ({
  t,
}) => {
  return (
    <Wrapper>
      <Img
        width="100"
        height="100"
        alt={t("photoAlt")}
        src="/static/images/alexander_kachkaev.jpg?"
      />
    </Wrapper>
  );
};

export default withNamespaces(["index"])(Photo);
