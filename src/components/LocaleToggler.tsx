import { UrlLike } from "next/router";
import React from "react";
import styled from "styled-components";

import { HostByLocale } from "../types";
import Locale from "./Locale";

const Wrapper = styled.div`
  position: absolute;
  top: 12px;
  right: 25px;
`;

const LocaleToggler = ({
  hostByLocale,
  url,
}: {
  hostByLocale: HostByLocale;
  url: UrlLike;
}) => (
  <Wrapper>
    <Locale targetLocale="en" host={hostByLocale["en"]} currentUrl={url} />{" "}
    <Locale targetLocale="ru" host={hostByLocale["ru"]} currentUrl={url} />
  </Wrapper>
);

export default LocaleToggler;
