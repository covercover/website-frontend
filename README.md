# \[[en.](https://en.kachkaev.ru)][kachkaev.ru](https://kachkaev.ru) source code: frontend microservice

See https://gitlab.com/kachkaev/website for the description of the overall website architecture.

This repo produces a microservice that is responsible for running the user-facing web app.
It is meant to work behind a reverse proxy together with the [GraphQL server](https://gitlab.com/kachkaev/website-graphql-server) microservice.

## Key ingredients

- **[TypeScript](https://www.typescriptlang.org/)** to ensure the highest code quality
- **[Node.js](https://nodejs.org/)** to run JavaScript on the server
- **[Next.js](https://github.com/zeit/next.js)** to bundle source files and render web pages both on the server and the client
- **[React](https://reactjs.org/)** to describe interface components
- **[Apollo GraphQL client](https://github.com/apollographql/apollo-client)** to receive social profile infos from the [GraphQL server](https://gitlab.com/kachkaev/website-graphql-server) microservice
- **[Styled components](https://www.styled-components.com/)** to produce well-structured and collision-free CSS
- **[I18next](https://github.com/i18next/i18next)** to support English and Russian translations
- **[Lodash](https://lodash.com/)** to leverage common utility functions
- **[ESLint](https://eslint.org/)** and **[Prettier](https://prettier.io/)** to ensure that source files are error-free and easy to read
- **[Docker](https://www.docker.com/)** to make the production version of the microservice straightforward to deploy
- **[GitLab CI](https://about.gitlab.com/features/gitlab-ci-cd/)** to automatically check code quality and generate a new docker image on every push

## Project structure

The repository generally follows [Next.js](https://github.com/zeit/next.js) folder structure, except that the source code is stored in `src` subdirectory.
This makes it easier to search and replace text across multiple files as well as to run code quality checks.

The code is inspired by these examples:

- [custom-server-typescript](https://github.com/zeit/next.js/tree/canary/examples/custom-server-typescript)
- [root-static-files](https://github.com/zeit/next.js/tree/canary/examples/root-static-files)
- [with-apollo](https://github.com/zeit/next.js/tree/canary/examples/with-apollo)
- [with-loading](https://github.com/zeit/next.js/tree/canary/examples/with-loading)
- [with-next-i18next](https://github.com/zeit/next.js/tree/canary/examples/with-next-i18next)
- [with-styled-components](https://github.com/zeit/next.js/tree/canary/examples/with-styled-components)

## Running a local copy

1.  Ensure you have the latest git, Node.js and Yarn installed on your machine

    ```bash
    git --version
    ## ≥ 2.14

    node --version
    ## ≥ 10.0

    yarn --version
    ## ≥ 1.10
    ```

1.  Clone the repo from GitLab

    ```bash
    cd PATH/TO/MISC/PROJECTS
    git clone https://gitlab.com/kachkaev/website-frontend.git
    cd website-frontend
    ```

1.  Install npm dependencies using Yarn

    ```bash
    yarn
    ```

1.  Copy `.env.dist` to `.env`.
    You can have a look at [`src/config.ts`](src/config.ts) for hints on what is expected.
    You normally don’t need to edit `.env` for this project, because by default `GRAPHQL_URI` points to the production server and `HOST_RU` and `HOST_EN` are ignored during development.
    If you would like to run the frontend together with a local copy of the [GraphQL server](https://gitlab.com/kachkaev/website-graphql-server), `.env` will need to contain `GRAPHQL_URI=http://localhost:4000` (assuming you did not change the default port).

1.  Start the server in development mode

    ```bash
    yarn dev
    ```

    Modifying any of the files will refresh the app, thanks to Next.js hot module reloading.
    To stop running the server, press `ctrl+c`.

1.  If you want to test a production copy of the microservice, build and run it like this:

    ```bash
    yarn build
    yarn start
    ```

## I18n (internationalization)

The website supports two locales: English and Russian.
React components do not contain any language-specific text and only refer to the ids of translation messages.
These messages are stored in `locales/(en|ru)/*.json` and are loaded by [react-i18next](https://github.com/i18next/react-i18next) both on the server and the client.
[ICU](http://userguide.icu-project.org/formatparse/messages) message format is used to handle variable injection and plurals.

### Configuring website locale

1.  Set `HOST_EN` & `HOST_RU` environment variables before starting the server (useful in production).

1.  Set `ENFORCED_LOCALE` to `en` or `ru` (useful in development, has higher priority than `HOST_EN`/`HOST_RU`).

If none of the above variables are set, a fallback `en` locale is used.

### Development workflow

1.  Start the development server:

    ```bash
    yarn dev
    ```

1.  Mention a new i18n key somewhere in the interface either with `t('key')` or `<Trans i18nKey="key" />`.

1.  Notice the new key in an autogenerated file `locales/en/[namespace].missing.json`.

1.  Replace a placeholder message with a meaningful one and move the value to `locales/en/[namespace].json`.

1.  Observe the message in the refreshed interface and go to step 2 if more messages need to be added.

1.  Once a new part of the interface has been completed, copy new or modified messages to `locales/ru/[namespace].json` and test the result using

    ```bash
    ENFORCED_LOCALE=ru yarn dev
    ```

    You can use `git diff` to track what has been changed in `locales/en`.
    This will help keeping `locales/ru` in sync.

## Getting involved

This is a pretty small personal project, meaning that there is nothing much to collaborate on.
However, I don’t exclude that you might want to learn something by playing with the repo or even make your own (much better) website based on my code.
If you have questions, feel free to ask me anything by creating a new [GitLab issue](https://gitlab.com/kachkaev/website-frontend/issues) or [sending an email](mailto:alexander@kachkaev.ru)!

The code is shared under the [MIT license](LICENSE), so you are free to do what you want with it!
